# OCULIGHT tutorial #

This is a tutorial to daylight simulations with [OCULIGHT](https://www.oculightdynamics.com/) based on the [Radiance raytracer](https://www.radiance-online.org/).

### What is this repository for? ###

This repository provides python scripts for Rhino users.
These scripts will help you prepare the files you need to submit your project to our cloud platform.

### How do I get set up? ###

1. You need to install the following program:
	* [Rhinoceros 6](https://www.rhino3d.com/) (or version 5)
1. Download the following python scripts from this repository:
	* OCULIGHT_AddLayers.py
	* OCULIGHT_ExportFiles.py

*The Grasshopper extension for Rhinoceros can be used to create the model geometry and generate sensor points, however, it is not described in this tutorial.*

### How to run a python script in Rhinoceros? ###

**Follow these steps:**

1. Go to “Tools” to activate a drop-down menu, select “Commands”, and click-on “Search…”
1. Type in the “Command Search”: RunPythonScript.
1. Select the appropriate script and click-on “Open”.

For more details read [this guide](https://developer.rhino3d.com/guides/rhinopython/python-running-scripts/).

### Preparation of the 3D model in Rhinoceros ###

**Setting up Rhinoceros:**

* Check units (mandatory): Go to “File” to activate a drop-down menu and click-on “Settings”. Go to “Units and tolerances” at “Model units” and select “Meters”, which is the unit this tutorial is using.
* Change view style (recommended for layer recognition): Go to “View” to activate a drop-down menu and select “Ghosted”. This view style can help you recognize layers while assigning geometry to OCULIGHT layers.

**Create your model geometry:**

* Orientation: Make sure that the positive y-axis is North.
* Layers: Keep geometry with different materials on separate layers.
* Geometry:
	* Geometry can include Mesh and Nurb, but you need to explode any block objects.
	* Glass should be modeled using single surfaces (no boxes).
	* Make a ground plane that extends at least 30 meters around the building in every direction.

### Preparation of files for OCULIGHT ###

1. Add OCULIGHT layers by running the script OCULIGHT_AddLayers.py
	* This script adds OCULIGHT layers to your project used for exporting the necessary files required for the simulations.
1. Assign geometry to OCULIGHT materials:
	* Select all surfaces with the same material property and choose the appropriate OCULIGHT layer name for the surfaces.
	* Do this for every surface.
1. Create viewpoints and assign to appropriate OCULIGHT sensor group:
	* Create a grid of point objects at eye level (or 1.6 meters off the floor) with a 2 m grid spacing.
	* These point locations will be used as view positions for the human-centric analysis.
	* This can be done manually or use the Grasshopper extension to generate grid of points.
1. Export your 3D model geometry and sensor points by running the script OCULIGHT_ExportFiles.py
	* Hidden and locked layers will not be exported.
	* Surfaces on unknown layers are automatically assigned a default material.
	* Points on sublayers of “OCULIGHT sensor groups::viewpoints” will only be exported and the rest ignored if any.
	* Pick a testpoint close to a south-facing window.

### Verify the content of your project ZIP file ###

With geometry files and sensor points successfully exported, we recommend you verify the content of your project ZIP file before submitting it.
Files are exported to a new folder called “OCULIGHT_%date_%time” found in the same directory as your Rhino project.

**Go to the “OCULIGHT_%date_%time” folder found in the same directory as your Rhino project and verify:**

* If the files are successfully exported. The folder should contain four files:
	* 3Dmodel
		* model.obj
		* model.mtl
	* sensors
		* viewpoints.csv
		* testpoints.csv
* If the OBJ 3D model can be viewed using [this online viewer](https://3dviewer.net/).
* If the CSV sensors files can be viewed using a spreadsheet program, such as Microsoft Excel, OpenOffice Calc, or Google Sheets.
