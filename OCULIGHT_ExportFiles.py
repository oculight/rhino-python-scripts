################################################################################
# OCULIGHT Export Files
#
# This file prepares 3D geometry and points for cloud simulations with OCULIGHT
################################################################################

# Copyright (C) 2019, OCULIGHT dynamics Sarl. All rights reserved.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import datetime
import zipfile
import Rhino
import scriptcontext
import System.Guid, System.Drawing.Color
import rhinoscriptsyntax as rs

def GetOBJSettings():
    e_str = "_Geometry=_Mesh "
    e_str+= "_EndOfLine=CRLF "
    e_str+= "_ExportRhinoObjectNames=DoNotExportObjectNames "
    e_str+= "_ExportRhinoGroupOrLayerNames=ExportLayersAsOBJGroups "
    e_str+= "_SortByOBJGroups=Yes "
    e_str+= "_ExportMaterialDefinitions=Yes "
    e_str+= "_ChangeWhitespaceToUnderscores=Yes "
    e_str+= "_YUp=No "
    e_str+= "_WrapLongLines=Yes "
    e_str+= "_WritePrecision=6 "
    e_str+= "_ExportMeshTextureCoordinates=Yes "
    e_str+= "_ExportMeshVertexNormals=Yes "
    e_str+= "_ExportMeshVertexColors=No "
    e_str+= "_ExportOpenMeshes=Yes "
    e_str+= "_VertexWelding=Unmodified "
    e_str+= "_CreateNGons=Yes "
    e_str+= "_MinFaceCount=2 "
    e_str+= "_IncludeUnWeldedEdges=Yes "
    e_str+= "_CullUnnecessaryVertexes=Yes "

    e_str+= "_Enter _Enter"

    return e_str

def ExportPointsOnSubLayers(sensorLayerTitle, exportPoints, filePath):
    '''Export points on layers'''

    parentLayer = sensorLayerTitle+'::'+exportPoints
    layerIndex = scriptcontext.doc.Layers.Find(parentLayer, True)

    fileName = os.path.join(filePath, exportPoints+'.csv')

    # Sublayer counter
    count_sLayer = 0
    
    # Get sublayers
    subLayers = rs.LayerChildren(parentLayer)
    # If any sublayers then
    if(subLayers):
        with open(fileName, "w") as file:
            file.write("x,y,z,floor,program,zone\n")
            
            for sLayer in subLayers:
                
                # Get tags from sublayer name
                tags = (sLayer[len(parentLayer+'::'):]).split('_')

                # Get all of the objects on the sublayer
                rhobjs = rs.ObjectsByLayer(sLayer, True)
                if rhobjs:
                    for id in rhobjs:
                        if( rs.IsPointCloud(id) ):
                            points = rs.PointCloudPoints(id)
                            for pt in points:
                                file.write(str(pt)+','+tags[0]+','+tags[1]+','+tags[2]+'\n')
                        elif( rs.IsPoint(id) ):
                            point = rs.PointCoordinates(id)
                            file.write(str(point)+','+tags[0]+','+tags[1]+','+tags[2]+'\n')
                    count_sLayer = count_sLayer + 1
                else:
                    print("No points found on layer "+str(sLayer))
    
    # No sublayers
    elif layerIndex>=0:
        with open(fileName, "w") as file:
            
            # Get all of the objects on the layer
            rhobjs = rs.ObjectsByLayer(exportPoints, True)
            if rhobjs:
                for id in rhobjs:
                    if( rs.IsPointCloud(id) ):
                        points = rs.PointCloudPoints(id)
                        for pt in points:
                            file.write(str(pt)+'\n')
                    elif( rs.IsPoint(id) ):
                        point = rs.PointCoordinates(id)
                        file.write(str(point)+'\n')
            else:
                print("No points found on layer "+str(exportPoints))
    
    return count_sLayer

if __name__=="__main__":
    
    # File settings
    fileName = rs.DocumentName()
    filePath = rs.DocumentPath().rstrip(fileName)
    basename = 'OCULIGHT'
    suffix = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
    exportFolderName = "_".join([basename, suffix])

    #
    # OBJ export
    #

    newDir = os.path.join(filePath, exportFolderName, '3Dmodel')
    if not os.path.exists(newDir):
        os.mkdir(newDir)
    
    exportFileName = os.path.join(newDir, 'model.obj')
    e_str = GetOBJSettings()

    rs.AllObjects(select=True)
    exportSuccess = rs.Command("_-Export "+exportFileName+" "+e_str)
    rs.AllObjects(select=False)

    # Simple message dialog
    if exportSuccess:
        rs.MessageBox("3D model successfully exported.", title="OCULIGHT OBJ export")

    #
    # Point export
    #

    newDir = os.path.join(filePath, exportFolderName, 'sensors')
    if not os.path.exists(newDir):
        os.mkdir(newDir)
    
    # Parent layer name
    sensorLayerTitle = 'OCULIGHT sensor groups'

    # Get sublayers
    subLayers = rs.LayerChildren(sensorLayerTitle)
    # If any sublayers then
    if(subLayers):
        Nviewpoints = 0
        Nworkplane = 0
        for sLayer in subLayers:
            if sLayer==sensorLayerTitle+'::viewpoints':
                # Export viewpoints
                Nviewpoints = ExportPointsOnSubLayers(sensorLayerTitle, 'viewpoints', newDir)
            elif sLayer==sensorLayerTitle+'::workplane':
                # Export workplane
                Nworkplane = ExportPointsOnSubLayers(sensorLayerTitle, 'workplane', newDir)
            else:
                print('Layer not recognized: '+sLayer)

        # Simple message dialog
        rs.MessageBox("Sensor points successfully exported for "+str(Nviewpoints)+" sublayer(s) in viewpoints and "+str(Nworkplane)+" sublayer(s) in workplane.", title=sensorLayerTitle)

    # Export testpoints given user input
    # A Yes, No dialog
    selectTestpoints = rs.MessageBox("Select a single viewpoint for testing the OBJ export?", 4 | 32)
    # If Yes
    if selectTestpoints==6:
        testpoints = rs.GetPointCoordinates(message="Select a point", preselect=False)
        if testpoints:
            testfileName = os.path.join(newDir, 'testpoints.csv')
            with open(testfileName, "w") as file:
                for point in testpoints:
                    print('Testpoint coordinates exported:')
                    print(point)
                    file.write(str(point)+'\n')

    #
    # Create ZIP folder
    #

    # A Yes, No dialog
    createZIP = rs.MessageBox("Compress exported files to a ZIP file?", 4 | 32)
    # If Yes
    if createZIP==6:
        zipFilePath = os.path.join(filePath, exportFolderName)
        zipFileName = os.path.join(filePath, exportFolderName+'.zip')

        zip_file = zipfile.ZipFile(zipFileName, 'w')
        print('Create a ZIP file...')
        for files in os.walk(zipFilePath):
            for fileName in files[2]:
                if fileName=='viewpoints.csv' or fileName=='workplane.csv' or fileName=='testpoints.csv':
                    print('adding... '+fileName)
                    actual_file_path = os.path.join(files[0], fileName)
                    zipped_file_path = os.path.join(exportFolderName,'sensors', fileName)
                    zip_file.write(actual_file_path, zipped_file_path)
                if fileName=='model.obj' or fileName=='model.mtl':
                    print('adding... '+fileName)
                    actual_file_path = os.path.join(files[0], fileName)
                    zipped_file_path = os.path.join(exportFolderName,'3Dmodel', fileName)
                    zip_file.write(actual_file_path, zipped_file_path)
        zip_file.close()
