################################################################################
# OCULIGHT Add Layers
#
# This file adds layers for material assignment and grouping of sensor points
################################################################################

# Copyright (C) 2019, OCULIGHT dynamics Sarl. All rights reserved.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import Rhino
import scriptcontext
import System.Guid, System.Drawing.Color
import rhinoscriptsyntax as rs

def AddLayer(parent_layer, layer_name, material_color, layer_color, material_trans=0.0):

    if not rs.IsLayer(parent_layer):
        rs.AddLayer(parent_layer)

    # Add a new layer to the document
    layer_index = rs.AddLayer(layer_name, color=layer_color, parent=parent_layer)
    if layer_index<0:
        print("Unable to add", layer_name, "layer")
        return Rhino.Commands.Result.Failure

    # Assign a material to the new layer
    material_index = rs.AddMaterialToLayer(layer_name)
    rs.MaterialColor(material_index, material_color)
    rs.MaterialTransparency(material_index, material_trans)

    return Rhino.Commands.Result.Success

def AddSensorLayer(parent_layer, layer_name, material_color, layer_color, add_layer):

    # Add a new sensor point layer
    AddLayer(parent_layer, 'viewpoints', material_color, System.Drawing.Color.Black)
    AddLayer('viewpoints', layer_name, material_color, layer_color)

    if add_layer:
        AddLayer(parent_layer, 'workplane', material_color, System.Drawing.Color.Black)
        AddLayer('workplane', layer_name, material_color, layer_color)

if __name__=="__main__":
    
    #
    # MATERIALS
    #

    mat_options = [['default (recommended)', True], 
                   ['glazings', False], 
                   ['grayscale', False], 
                   ['paint colors', False], 
                   ['luminaires (advanced users)', False],
                   ['textures (advanced users)', False],
                   ['custom (advanced users)', False]]
    mat_results = rs.CheckListBox(mat_options, "Pick an option", "OCULIGHT materials")
    if mat_results:
        mat_results_sum = sum([item[1] for item in mat_results])
        for mat_layer in mat_results:
            if mat_layer[1] and mat_layer[0]==mat_options[0][0]:
                # OCULIGHT default materials
                AddLayer("OCULIGHT default materials", 'context_buildings_30', (77,77,77), System.Drawing.Color.Black)
                AddLayer("OCULIGHT default materials", 'context_ground_20', (51,51,51), System.Drawing.Color.Gray)
                AddLayer("OCULIGHT default materials", 'context_vegetation_10', (26,26,26), System.Drawing.Color.Green)
                AddLayer("OCULIGHT default materials", 'floor_20', (51,51,51), System.Drawing.Color.Red)
                AddLayer("OCULIGHT default materials", 'furniture_50', (128,128,128), System.Drawing.Color.Brown)
                AddLayer("OCULIGHT default materials", 'glass_Td_70', (168,204,215), System.Drawing.Color.Magenta, 0.7)
                AddLayer("OCULIGHT default materials", 'interior_ceiling_70', (179,179,179), System.Drawing.Color.Yellow)
                AddLayer("OCULIGHT default materials", 'interior_wall_50', (128,128,128), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT default materials", 'window_frames_70', (179,179,179), System.Drawing.Color.Cyan)

            if mat_layer[1] and mat_layer[0]==mat_options[1][0]:
                # OCULIGHT glazings
                AddLayer("OCULIGHT glazings", 'glass_single_clear_Td_82', (168,204,215), System.Drawing.Color.Magenta, 0.82)
                AddLayer("OCULIGHT glazings", 'glass_double_clear_Td_70', (168,204,215), System.Drawing.Color.Magenta, 0.7)
                AddLayer("OCULIGHT glazings", 'glass_triple_clear_Td_66', (168,204,215), System.Drawing.Color.Magenta, 0.66)
                AddLayer("OCULIGHT glazings", 'glass_Td_45', (168,204,215), System.Drawing.Color.Magenta, 0.45)
                AddLayer("OCULIGHT glazings", 'glass_Td_65', (168,204,215), System.Drawing.Color.Magenta, 0.65)
                AddLayer("OCULIGHT glazings", 'glass_Td_80', (168,204,215), System.Drawing.Color.Magenta, 0.8)
                AddLayer("OCULIGHT glazings", 'glass_Td_88', (168,204,215), System.Drawing.Color.Magenta, 0.88)
                AddLayer("OCULIGHT glazings", 'ec_clear_60', (168,204,215), System.Drawing.Color.Magenta, 0.6)
                AddLayer("OCULIGHT glazings", 'ec_tinted_30', (168,204,215), System.Drawing.Color.Magenta, 0.3)
                AddLayer("OCULIGHT glazings", 'ec_tinted_02', (168,204,215), System.Drawing.Color.Magenta, 0.02)

            if mat_layer[1] and mat_layer[0]==mat_options[2][0]:
                # OCULIGHT grayscale materials
                AddLayer("OCULIGHT grayscale materials", 'context_buildings_35', (89,89,89), System.Drawing.Color.Black)
                AddLayer("OCULIGHT grayscale materials", 'floor_10', (26,26,26), System.Drawing.Color.Red)
                AddLayer("OCULIGHT grayscale materials", 'floor_30', (77,77,77), System.Drawing.Color.Red)
                AddLayer("OCULIGHT grayscale materials", 'floor_40', (102,102,102), System.Drawing.Color.Red)
                AddLayer("OCULIGHT grayscale materials", 'furniture_20', (51,51,51), System.Drawing.Color.Brown)
                AddLayer("OCULIGHT grayscale materials", 'furniture_80', (204,204,204), System.Drawing.Color.Brown)
                AddLayer("OCULIGHT grayscale materials", 'interior_60', (153,153,153), System.Drawing.Color.Orange)
                AddLayer("OCULIGHT grayscale materials", 'interior_70', (179,179,179), System.Drawing.Color.Orange)
                AddLayer("OCULIGHT grayscale materials", 'interior_80', (204,204,204), System.Drawing.Color.Orange)
                AddLayer("OCULIGHT grayscale materials", 'interior_90', (230,230,230), System.Drawing.Color.Orange)

            if mat_layer[1] and mat_layer[0]==mat_options[3][0]:
                # OCULIGHT paint colors
                AddLayer("OCULIGHT paint colors", 'paint_blue_light', (97,191,235), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_green_light', (161,196,56), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_green_medium', (85,189,105), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_grey_dark', (66,74,74), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_red_light', (209,82,20), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_red_medium', (186,26,26), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_rose_light', (179,120,161), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_rose_medium', (173,89,140), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_violet_light', (138,120,181), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_violet_medium', (94,54,92), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_white_glossy', (128,128,128), System.Drawing.Color.Purple)
                AddLayer("OCULIGHT paint colors", 'paint_white_pure', (255,255,255), System.Drawing.Color.Purple)

            if mat_layer[1] and mat_layer[0]==mat_options[4][0]:
                # OCULIGHT luminaires
                AddLayer("OCULIGHT luminaires", 'cylinderrecessed_light', (128, 128, 128), System.Drawing.Color.Cyan)
                AddLayer("OCULIGHT luminaires", 'linearluminaire_light', (128, 128, 128), System.Drawing.Color.Cyan)
            
            if mat_layer[1] and mat_layer[0]==mat_options[5][0]:
                # OCULIGHT textures
                AddLayer("OCULIGHT textures", 'wavy_water_glass', (175, 238, 238), System.Drawing.Color.Blue, 0.5)
                AddLayer("OCULIGHT textures", 'wavy_water_surface', (175, 238, 238), System.Drawing.Color.Blue, 0.5)

            if mat_layer[1] and mat_layer[0]==mat_options[6][0]:
                # OCULIGHT custom materials
                AddLayer("OCULIGHT custom materials", 'custom_no1', (128, 128, 128), System.Drawing.Color.Gray)
                AddLayer("OCULIGHT custom materials", 'custom_no2', (128, 128, 128), System.Drawing.Color.Gray)
    else:
        print("OCULIGHT materials canceled!")
        mat_results_sum = 0
    
    #
    # SENSOR POINTS
    #

    sensorLayerColor = [System.Drawing.Color.Blue,
                        System.Drawing.Color.Brown, 
                        System.Drawing.Color.Cyan, 
                        System.Drawing.Color.Green,
                        System.Drawing.Color.Magenta,
                        System.Drawing.Color.Orange,
                        System.Drawing.Color.Purple,
                        System.Drawing.Color.Red,
                        System.Drawing.Color.Yellow]

    sensorLayerTitle = "OCULIGHT sensor groups"
    program_options = [['work', True], ['meet', True], ['move', True], ['play', True], ['relax', False], ['dine', False], ['sleep', False], ['mixed', False]]
    zone_options = [['core', False], ['perimeter', False], ['North', False], ['South', False], ['East', False], ['West', False], ['public', False], ['private', False]]

    # Group by floors
    Nfloors = rs.RealBox("Enter number of floors to be analyzed", 1 , title=sensorLayerTitle, minimum=1, maximum=3)

    if Nfloors:
        # Group by program (occupant activity)
        program_results = rs.CheckListBox(program_options, message="Select program tags:\n Used for grouping point locations based\n on the function of each space in your model", title=sensorLayerTitle)
        program_tags = []
        if program_results:
            for program in program_results:
                if program[1]:
                    program_tags.append(program[0])

            Nprograms = len(program_tags)
            if Nprograms==0:
                print("No program tags selected!")
                program_tags.append('')
                Nprograms = 1
        else:
            print("Unable to add program tags!")
            program_tags.append('')
            Nprograms = 1
        
        # Group by zone (optional)
        zone_results  = rs.CheckListBox(zone_options, message="Additional optional tags", title=sensorLayerTitle)
        zone_tags = []
        if zone_results:
            for zone in zone_results:
                if zone[1]:
                    zone_tags.append(zone[0])
            Nzones = len(zone_tags)
            if Nzones==0:
                print("No optional zone tags selected!")
                zone_tags.append('')
                Nzones = 1
        else:
            print("Unable to add optional zone tags!")
            zone_tags.append('')
            Nzones = 1
    
        # Add workplane? A Yes, No dialog
        sameGridSpacing = rs.MessageBox("Will you use the same grid spacing for viewpoints and workplane?", 4 | 32)
        if sameGridSpacing==6:
            addWorkplane = False
        else:
            addWorkplane = True
        
        for floor in range(0,int(Nfloors)):
            color_count = 0
            for program in program_tags:
                for zone in zone_tags:
                    tmp_str = str(floor+1)+'_'+str(program)+'_'+str(zone)

                    # Add OCULIGHT sensor points
                    AddSensorLayer(sensorLayerTitle, tmp_str, (1, 1, 1), sensorLayerColor[color_count], addWorkplane)

                    # Set layer color
                    if color_count==len(sensorLayerColor):
                        color_count = 0
                    else:
                        color_count = color_count+1
    else:
        print("OCULIGHT sensor groups canceled!")

    #
    # MESSAGE
    #

    # Simple message dialog
    if mat_results_sum>0 and Nfloors:
        rs.MessageBox("OCULIGHT layers added for materials and sensor groups.", title="OCULIGHT layers")
    elif mat_results_sum>0:
        rs.MessageBox("OCULIGHT layers added for materials only.", title="OCULIGHT layers")
    elif Nfloors:
        rs.MessageBox("OCULIGHT layers added for sensor groups only.", title="OCULIGHT layers")
    else:
        rs.MessageBox("Unable to add OCULIGHT layers.", title="OCULIGHT layers")
